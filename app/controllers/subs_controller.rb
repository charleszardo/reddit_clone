class SubsController < ApplicationController
  before_filter :redirect_if_not_owner, only: [:edit, :update]

  def index
    @subs = Sub.all
    render :index
  end

  def new
    @sub = Sub.new
    render :new
  end

  def create
    @sub = Sub.new(sub_params)
    if @sub.save
      redirect_to subs_url(@sub)
    else
      flash[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def edit
    @sub = Sub.find(params[:id])
    render :edit
  end

  def update
    @sub = Sub.find(params[:id])
    if @sub.update(sub_params)
      redirect_to subs_url(@sub)
    else
      flash[:errors] = @sub.errors.full_messages
      @sub = Sub.new if @sub.nil?
      render :edit
    end
  end

  def show
    @sub = Sub.find(params[:id])
    render :show
  end

  private
  def sub_params
    params.require(:sub).permit(:title, :description, :moderator_id)
  end

  def is_owner?
    current_user.id == Sub.find(params[:id]).moderator_id
  end

  def redirect_if_not_owner
    unless is_owner?
      flash[:errors] = ["You're not the moderator!"]
      redirect_to sub_url(Sub.find(params[:id]))
    end
  end
end