class PostsController < ApplicationController
  before_filter :redirect_if_not_owner, only: [:edit, :update]

  def new
    @post = Post.new
    render :new
  end

  def create
    @post = Post.new(post_params)
    @post.sub_ids = params[:post_sub][:sub_id]
    if @post.save
      redirect_to post_url(@post)
    else
      #fail
      flash[:errors] = @post.errors.full_messages
      render :new
    end
  end

  # transaction do
#     if @post.save
#       sub_ids = params[:post_sub][:sub_id]
#       sub_ids.each do |sub_id|
#         PostSub.create!(post_id: @post.id, sub_id: sub_id)
#       end
#     end
#   end

  def edit
    @post = Post.find(params[:id])
    render :edit
  end

  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)
      redirect_to post_url(@post)
    else
      flash[:errors] = @post.errors.full_messages
      @post = Post.new(post_params) if @post.nil?
      render :edit
    end
  end

  def show
    @post = Post.find(params[:id])
    render :show
  end

  def destroy
  end

  private
  def post_params
    params.require(:post).permit(:title, :content, :url,
                                 :author_id)
  end

  def is_owner?
    current_user.id == Post.find(params[:id]).author_id
  end

  def redirect_if_not_owner
    unless is_owner?
      flash[:errors] = ["You're not the author!"]
      redirect_to post_url(Post.find(params[:id]))
    end
  end

end