class SessionsController < ApplicationController

  def new
    # set something?
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_credentials(params[:user][:email],
                                  params[:user][:password])
    unless @user.nil?
      log_in!(@user)
      redirect_to subs_url
    else
      flash[:errors] = ["Incorrect email or password!"]
      @user = User.new
      render :new
    end
  end

  def destroy

    log_out!
    redirect_to new_session_url
  end

end