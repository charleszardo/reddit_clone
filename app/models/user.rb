class User < ActiveRecord::Base
  validates :email, :session_token, :password_digest, presence: true
  validates :email, :session_token, uniqueness: true

  after_initialize :ensure_session_token

  has_many :subs,
    class_name: "Sub",
    foreign_key: :moderator_id,
    primary_key: :id,
    inverse_of: :moderator

  has_many :posts,
    class_name: "Post",
    foreign_key: :author_id,
    primary_key: :id,
    inverse_of: :author

  def self.generate_session_token(field)
    begin
      token = SecureRandom::urlsafe_base64(16)
    end until !self.exists?(field => token)
    token
  end

  def self.find_by_credentials(email, password)
    user = User.find_by(email: email)

    return nil if user.nil?

    user.is_password?(password) ? user : nil
  end

  def ensure_session_token
    self.session_token ||= User.generate_session_token(:session_token)
  end

  def reset_session_token!
    self.session_token = User.generate_session_token(:session_token)
    self.save!
    self.session_token
  end

  def password=(reg_password)
    self.password_digest = BCrypt::Password.create(reg_password)
  end

  def is_password?(reg_password)
    BCrypt::Password.new(self.password_digest).is_password?(reg_password)
  end

end
