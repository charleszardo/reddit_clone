class PostSub < ActiveRecord::Base
  #validates :post_id, presence: true  #:sub_id,
  validates :post_id, uniqueness: { scope: :sub_id }

  belongs_to :post,
    class_name: "Post",
    foreign_key: :post_id,
    primary_key: :id,
    inverse_of: :post_subs

  belongs_to :sub,
    class_name: "Sub",
    foreign_key: :sub_id,
    primary_key: :id,
    inverse_of: :post_subs
end

